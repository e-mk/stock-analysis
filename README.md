# Stock Analysis

Service that uses JSON-RPC instead usual endpoint under HTTP verbs api design.

# Use case

Apply analysis over Brazilian stocks;

# Purporses

Explore Rust World!

- Language features;
- Error handling;
- Cargo;
- Deployment;
- Modules (focused in project organization);
- Others (that I don't know yet hehe).

# BRApi

This projects uses `brapi`. A REST API that allows us to obtain stocks details for free.

More about that service can be found in their [GitHub](https://github.com/Alissonsleal/brapi) or [Official About Page](https://brapi.ga/about).

# Try application

>Don't forget it: you should have Rust setup ready to develop!
>
>Do these very simple steps:
>
>`$ git clone git@gitlab.com:e-mk/stock-analysis.git`
>
> `$ cd stock-analysis`
>
> `$ cargo run`
>
>Try some requests with the examples that could be found in `request_example` directory;

## Developer

[Marcos R. Araujo](https://www.linkedin.com/in/marcos-araujo-11107124/)

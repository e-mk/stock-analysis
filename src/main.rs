use std::error::Error;
use std::fmt::Display;
use jsonrpc_core::ErrorCode;
use jsonrpc_core::serde_json;
use jsonrpc_core::IoHandler;
use jsonrpc_core::Params;
use jsonrpc_core::Value;
use jsonrpc_http_server::cors::AccessControlAllowHeaders;
use jsonrpc_http_server::RestApi;
use jsonrpc_http_server::ServerBuilder;
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct GetQuoteByCodeParams {
    code: String,
}

#[macro_use]
extern crate log;

#[tokio::main]
async fn main() {
    env_logger::init();

    let mut io = IoHandler::default();

    io.add_method("quote_stock_by_code", |params: Params| async {
        let method_params: GetQuoteByCodeParams = params.parse().unwrap();

        log::debug!(
            "Handling quote_stock_by_code method with code {}.",
            &method_params.code
        );

        let stock_quotation_result = quote_stock_by_code(&method_params.code).await;

        match stock_quotation_result {
            Ok(res) => Ok(Value::String(serde_json::to_string(&res).unwrap())),
            Err(error) => {
                let error = jsonrpc_core::Error {
                    code : ErrorCode::ServerError(-320000),
                    message : error.message,
                    data: Some(Value::String(error.data))
                };
                Err(error)
            }
        }
    });

    let server_addr = build_server_addr();

    let server = ServerBuilder::new(io)
        .cors_allow_headers(AccessControlAllowHeaders::Any)
        .rest_api(RestApi::Unsecure)
        .start_http(&server_addr.parse().unwrap())
        .unwrap();

    info!("Server started at {}", &server_addr);

    server.wait();
}

#[derive(Debug, Serialize, Deserialize)]
struct StockQuotation {
    results: Vec<StockQuotationDetail>,
}

#[derive(Debug, Serialize, Deserialize)]
struct StockQuotationDetail {
    symbol: String,
    #[serde(rename(serialize = "shortName", deserialize = "shortName"))]
    short_name: String,
    logourl: String,
    #[serde(rename(
        serialize = "longName",
        deserialize = "longName"
    ))]
    long_name: String,
    currency: String,
    #[serde(rename(
        serialize = "regularMarketPrice",
        deserialize = "regularMarketPrice"
    ))]
    regular_market_price: Option<f64>,
    #[serde(rename(
        serialize = "twoHundredDayAverage",
        deserialize = "twoHundredDayAverage"
    ))]
    two_hundred_day_average: Option<f64>
}

// infrastructure
#[derive(Debug, Serialize, Deserialize)]
struct VendorError {
    message: String,
    data: String
}

impl Display for VendorError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({})", self.message)
    }
}

impl Error for VendorError {}

#[derive(Debug, Serialize, Deserialize)]
struct StockNotFound {
    error: String
}

async fn quote_stock_by_code(code: &String) -> Result<StockQuotation, VendorError> {
    let url = Some(format!(
        "https://brapi.ga/api/quote/{}?fundamental=true",
        code
    ));

    let req_result = reqwest::get(url.unwrap()).await.unwrap();

    match req_result.status() {
        StatusCode::OK => {
            match req_result.json::<StockQuotation>().await {
                Ok(res) => return Ok(res),
                Err(error) => {
                    Err(VendorError {
                        message: String::from("Parser error"),
                        data: error.to_string()
                    }) 
                }
            }
        },
        StatusCode::NOT_FOUND => {
            let resp = req_result.text().await.unwrap();
            return Err(VendorError {
                message: String::from(format!("Stock with code {} not found", code)),
                data: resp
            })
        },
        other => {
            info!("Unknow response from provider.");
            return Err(VendorError {
                message: String::from("unknown response from provider"),
                data: other.to_string()
            })
        }
    }
}

// config
fn build_server_addr() -> String {
    let server_domain = "127.0.0.1";
    let server_port = "9000";
    format!("{}:{}", server_domain, server_port)
}
